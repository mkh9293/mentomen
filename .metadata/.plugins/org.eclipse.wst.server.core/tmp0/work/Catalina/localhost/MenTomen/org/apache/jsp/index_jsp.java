/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.0.21
 * Generated at: 2015-11-29 08:35:52 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(1);
    _jspx_dependants.put("/header.jsp", Long.valueOf(1448783593521L));
  }

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

final java.lang.String _jspx_method = request.getMethod();
if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
return;
}

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\t \r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">\r\n");
      out.write("  <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("<div style=\"position:absolute; margin-right:3%; margin-top:1%; right:10px;\">\r\n");
 int name,level;
if(session.getAttribute("id")!=null){
	level=(int)session.getAttribute("level");
	name=(int)session.getAttribute("id"); 
      out.write("\r\n");
      out.write("\t<b>학번 : ");
      out.print(name);
      out.write("</b>\r\n");
      out.write("\t<button type=\"button\" class=\"btn btn-default\" onclick=\"javascript:window.location='./MemberLogout.me'\">로그아웃</button>\r\n");
      out.write("\t<button type=\"button\" class=\"btn btn-default\" onclick=\"javascript:window.location='./Mypage.rq'\">마이페이지</button>\r\n");
}else{ 
      out.write("\r\n");
      out.write("<button type=\"button\" class=\"btn btn-default\" onclick=\"javascript:window.location='./MemberLogin.me'\">로그인</button>\r\n");
      out.write("<button type=\"button\" class=\"btn btn-default\" onclick=\"javascript:window.location='./MemberJoin.me'\">회원가입</button>\r\n");
} 
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<link rel=\"stylesheet\" href=\"./css/index.css\">\r\n");
      out.write("<title>Insert title here</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("<div class=\"container-fluid\">\r\n");
      out.write("\t\t<div class=\"row-fluid\">\r\n");
      out.write("\t\t\t<img src=\"./img/banner2.png\" class=\"img-rounded\">\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t<div class=\"container\">\r\n");
      out.write("\t\t<h1>Man to men</h1>\r\n");
      out.write("\t\t<h5>Man To men 이란?\r\n");
      out.write(" 한 명의 멘토(Man)가 다수의 멘티들(men)에게 멘토링을 해준다는 의미로 학교 선배들이 진로를 고민하는 후배들에게 도움을 주고자, 본인의 프로그래밍 경험과 노하우를 나누는 프로그램입니다.\t</h5>\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<br>\r\n");
      out.write("\t\t<div id=\"banner-img2\">\r\n");
      out.write("\t\t<img src=\"./img/lec.png\" class=\"img-circle\" onClick=\"location.href='./BoardList.bo?pageVal=1'\">\r\n");
      out.write("\t\t<img src=\"./img/notice.png\" class=\"img-circle\" onClick=\"location.href='./BoardList.bo?pageVal=3'\">\r\n");
      out.write("\t\t<img src=\"./img/qna.png\" class=\"img-circle\" onClick=\"location.href='./BoardList.bo?pageVal=2'\">\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("\t<div>\r\n");
      out.write("\t\t<div style=\"display:table\" id=\"table-div\">\r\n");
      out.write("\t\t\t<div style=\"display:table-row\">\r\n");
      out.write("\t\t\t\t<div style=\"display:table-cell\" id=\"table-div2\">\r\n");
      out.write("\t\t\t\t\t<div style=\"display:table-row\" >\r\n");
      out.write("\t\t\t\t\t<div style=\"display:table-cell\" ><img src=\"./img/home.jpg\" onClick=\"location.href='http://skhu.ac.kr'\"></div><div style=\"display:table-cell\" ><img src=\"./img/lib.jpg\"  onClick=\"location.href='http://library.skhu.ac.kr/index.ax'\"></div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t<div style=\"display:table-row\" >\r\n");
      out.write("\t\t\t\t\t<div style=\"display:table-cell\" ><img src=\"./img/e.jpg\" onClick=\"location.href='http://ecareer.skhu.ac.kr/icons/app/cms/front.php'\"></div><div style=\"display:table-cell\" ><img src=\"./img/sw.jpg\" onClick=\"location.href='http://sw.skhu.ac.kr/'\"></div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t<div style=\"display:table-cell\"><img src=\"./img/mento.png\" class=\"img-rounded\">\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t</div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
