<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="mtm.board.db.*,java.util.*"%>
<%
	BoardBean board = (BoardBean) request.getAttribute("boarddata");
	List comment = (List)request.getAttribute("commentList");
%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="./css/qna_board.css" rel="stylesheet">
</head>
<body>
	<%@ include file="/../header.jsp"%>
	<br>
	<br>
	<!-- 글 내용 본문 -->
	<div class="container">
		<div class="row-fluid">
			<fieldset id="view_fieldset">
				<legend>Q&A게시판 글보기.</legend>
				<div>
					<span>
						<h4 id="view_h4">제목</h4>
					</span>
					<blockquote>
						<footer><%=board.getBOARD_SUBJECT()%></footer>
					</blockquote>
				</div>
				<div>
					<span>
						<h4>작성자</h4>
					</span>
					<blockquote id="view_blockquote">
						<footer><%=board.getBOARD_NAME()%></footer>
					</blockquote>
				</div>
				<hr>
				<div id="textarea-div">
					<%=board.getBOARD_CONTENT()%>
				</div>
				<hr>
				<div class="row-fluid" id="view_row">
					<%
						/*if (session.getAttribute("id") != null) {
							 	if (session.getAttribute("id").toString()
									.equals(board.getBOARD_NAME().toString())) { */
					%>
					<button class="btn btn-default" data-toggle="modal" 
						data-target="#myModal2">
						<span class="glyphicon glyphicon-pencil"></span> 수정
					</button>
					<button class="btn btn-default" data-toggle="modal"
						data-target="#myModal">
						<span class="glyphicon glyphicon-remove"></span> 삭제
					</button>
					<%
						//}
						//	}
					%>
					<button class="btn btn-default" id="reply_btn">
						<span class="glyphicon glyphicon-th-list"></span> 답변
					</button>
					<button class="btn btn-default" id="list_btn">
						<span class="glyphicon glyphicon-th-list"></span> 목록
					</button>

				</div>
			</fieldset>
		</div>
		<br> <br>
		<!-- 글 내용 본문 끝 -->
		
		
		<!-- 삭제버튼 클릭 시 나오는 폼 -->
		<!-- remove modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">삭제하기</h4>
					</div>
					<div class="modal-body">
						<form action="./BoardDelete.bo" method="post" name="deleteform">
							<input type="hidden" name="pageVal"
								value="<%=request.getAttribute("pageVal")%>"> <input
								type="hidden" name="page"
								value="<%=request.getAttribute("page")%>"> <input
								type="hidden" name="num" value="<%=board.getBOARD_NUM()%>">

							<input type="text" class="form-control" id="SUBJECT"
								name="BOARD_PASS" placeholder="글 비밀번호를 입력하세요.">

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="remove_btn">삭제</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- 삭제 버튼 클릭 시 나오는 폼 끝 -->
		
		
		<!-- 수정하기 버튼 클릭 시 나오는 폼 -->
		<!-- update modal -->
		<div class="modal fade" id="myModal2" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">수정하기</h4>
					</div>
					<div class="modal-body">
						<form action="./BoardModify.bo" method="post" name="updateform">
							<input type="hidden" name="pageVal"
								value="<%=request.getAttribute("pageVal")%>"> <input
								type="hidden" name="page"
								value="<%=request.getAttribute("page")%>"> <input
								type="hidden" name="num" value="<%=board.getBOARD_NUM()%>">

							<input type="text" class="form-control" id="SUBJECT"
								name="BOARD_PASS" placeholder="글 비밀번호를 입력하세요.">

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="change_btn">수정</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- 수정 버튼 클릭 폼 끝 -->
		
		
		<!-- 댓글 쓰기 폼 -->
	<div class="container" >
		<div class="row" >
		<form action="./CommentAddAction.bo" method="post">
		<input type="hidden" name="BOARD_ID" value="2">
		<input type="hidden" name="BOARD_NUM" value="<%=board.getBOARD_NUM()%>">
		<input type="hidden" name="page" value="<%=request.getAttribute("page")%>"> 
			<fieldset id="comment_fieldset">
				<legend>댓글</legend>
				<div class="form-group">
				<%if(session.getAttribute("id")!=null){ 
					name = (int)session.getAttribute("id");%>
				작성자:	<input type="text" id="CNAME" value="<%=name %>" readonly name="COMMENT_NAME" placeholder="아이디 또는 학번">
				<%}else{ %>
				작성자:	<input type="text" id="CNAME" name="COMMENT_NAME" placeholder="아이디 또는 학번">
				<%} %>&nbsp; 
				비밀번호: <input type="password" name="COMMENT_PASS" placeholder="비밀번호 입력">  
				<span>
				<button class="btn btn-default"
							id="comment_submit_btn">
							<span class="glyphicon glyphicon-ok"></span> 저장
						</button>
						<button class="btn btn-default"
							id="comment_reset_btn">
							<span class="glyphicon glyphicon-remove"></span> 취소
						</button>
				</span><br>
				<br>
				<textarea id="board_content" name="COMMENT_CONTENT" rows="4" placeholder="댓글을 작성해주세요." style="resize:none; width:100%"></textarea>
				</div>
			</fieldset>
		</form>
		</div>
	</div>
	<!-- 댓글 쓰기 폼 끝 -->
	
	<!-- 댓글 내용 -->
	<div class="container" id="comment_div">
		<div class="row">
		<%for(int i=0;i<comment.size();i++){
			CommentBean cm = (CommentBean) comment.get(i);%>
			<div class="panel panel-info">
				<div class="panel-heading">
					<%=cm.getCOMMENT_NAME() %>&nbsp;(<%=cm.getTIME()%>)
					<!-- <span id="comment_span"> -->
						<button class="btn btn-default" data-toggle="modal" 
						data-target="#myModal3">
						<span class="glyphicon glyphicon-pencil"></span> 수정
					</button>
					<button class="btn btn-default" data-toggle="modal" onclick="removeFunc(<%=cm.getCOMMENT_IDX()%>)"
						data-target="#myModal4">
						<span class="glyphicon glyphicon-remove"></span> 삭제
					</button><br>
						
					<!-- </span> -->
				</div>
			
				<div class="panel-body comment_num" value="<%=cm.getCOMMENT_IDX()%>">
					<%=cm.getCOMMENT_CONTENTS() %>
				</div>
			</div>
		<%} %> 
		</div>
	</div>
	<!-- 댓글 내용 끝 -->

	<!-- 댓글 수정하기 버튼 클릭 시 나오는 폼 -->
		<!-- update modal -->
		<div class="modal fade" id="myModal3" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">댓글 수정하기</h4>
					</div>
					<div class="modal-body">
						<form action="./CommentModify.bo" method="post" name="commentUpdateForm">
							<input type="hidden" name="pageVal"
								value="<%=request.getAttribute("pageVal")%>"> <input
								type="hidden" name="page"
								value="<%=request.getAttribute("page")%>"> <input
								type="hidden" name="num" value="<%=board.getBOARD_NUM()%>">

							<input type="text" class="form-control" id="password"
								name="COMMENT_PASS" placeholder="댓글 비밀번호를 입력하세요.">

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="change_comment_btn">수정</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- 댓글수정 버튼 클릭 폼 끝 -->
		
		<!-- 댓글 삭제하기 버튼 클릭 시 나오는 폼 -->
		<!-- update modal -->
		<div class="modal fade" id="myModal4" role="dialog">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">댓글 삭제하기</h4>
					</div>
					<div class="modal-body">
						<form action="./CommentDelete.bo" method="post" name="commentDeleteForm">
							<input type="hidden" name="pageVal"
								value="<%=request.getAttribute("pageVal")%>"> <input
								type="hidden" name="page"
								value="<%=request.getAttribute("page")%>"> <input
								type="hidden" name="commentN" id="commentN">

							<input type="text" class="form-control" id="password"
								name="COMMENT_PASS" placeholder="댓글 비밀번호를 입력하세요.">

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="remove_comment_btn">삭제</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		<!-- 댓글삭제 버튼 클릭 폼 끝 -->
	<script>
	function removeFunc(obj){
		var cl=document.getElementById("commentN");
		cl.value=obj;
	}
		$(document).ready(function(){
			$("#list_btn").click(function(){
				$(location).attr('href',"./BoardList.bo?pageVal=<%=request.getAttribute("pageVal")%>&page=<%=request.getAttribute("page")%>");
			});
			$("#reply_btn").click(function(){
				$(location).attr('href',"./BoardReplyView.bo?num=<%=board.getBOARD_NUM()%>&page=<%=request.getAttribute("page")%>");
			});
			$("#change_btn").click(function(){
				updateform.submit();
			});
			$("#change_comment_btn").click(function(){
				commentUpdateForm.submit();
			});
			$("#remove_btn").click(function(){
				deleteform.submit();	
			});
			$("#remove_comment_btn").click(function(){
				commentDeleteForm.submit();	
			});
			$("#comment_submit_btn").click(function(){
				submit();
			});
			$("#comment_reset_btn").click(function(){
				var content=document.getElementById("board_content");
				content.value='';
				return false;
			});
});		
</script>
</body>
</html>