$(document).ready(function(){
	$("#submit_btn").click(function() {
	    var title=document.getElementById("SUBJECT");
	    var name=document.getElementById("NAME");
	    if(title.value.trim()==''){
	    	alert("제목을 입력해주세요.");
	    	oEditors.getById["ir1"].exec("FOCUS", []);
	    	title.focus();
	    	return false;
	    } 
	    if(name.value.trim()==''){
	    	alert("작성자를 입력해주세요.");
	    	oEditors.getById["ir1"].exec("FOCUS", []);
	    	name.focus();
	    	return false;
	    }
	    else{
	        oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
	    	submit();
	    }
	});	

	$("#reset_btn").click(function(){
		history.go(-1);
		var title=document.getElementById("SUBJECT");
		var name=document.getElementById("NAME");
		title.value='';
		name.value='';
		oEditors.getById["ir1"].exec("SET_IR", [""]);
		return false;
	});
	
	var plusBtn = 1;
	$('.plus_btn').click(function(){
		plusBtn++;
		var forms='<div class="'+plusBtn+'"></div>';
		var div1='<div style="margin-top:1%;" value="'+plusBtn+'" class="col-sm-8 col-sm-offset-2 '+plusBtn+'"><input type="file" class="form-control" id="FILE" name="file'+plusBtn+'"></div>';
		var div2='<div style="margin-top:1%;" value="'+plusBtn+'" class="col-sm-2 '+plusBtn+'"><span class="btn btn-default" onclick="re('+plusBtn+');"><span class="glyphicon glyphicon-minus"></span> 삭제</span></div>';
		$('#plusForm:last').append(forms).append(div1).append(div2);
		return false;
	});

});		
function re(obj){
	$('#plusForm .'+obj).remove();
}
