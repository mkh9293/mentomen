<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MTM</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script>
function check(){
	
	var id=joinform.MEMBER_ID.value;
	var pw=joinform.MEMBER_PW.value;
	var pw2=joinform.MEMBER_PW2.value;
	var num_check=/^[0-9]*$/;
	
	var forms=document.getElementById("joinform");
	
	if(id.length==0){
		alert("아이디를 입력해주세요.");
		joinform.MEMBER_ID.focus();
		return false;
	}
	if(pw.length==0){
		alert("비밀번호를 입력해주세요.");
		joinform.MEMBER_PW.focus();
		return false;
	}
	if(pw!=pw2){
		alert("비밀번호가 일치하지 않습니다.");
		joinform.MEMBER_PW.value="";
		joinform.MEMBER_PW2.value="";
		joinform.MEMBER_PW.focus();
		return false;
	}
	if((forms.MEMBER_NAME.value=="")||(forms.MEMBER_NAME.value.length<=1)){
		alert("이름을 제대로 입력해주세요.");
		forms.MEMBER_NAME.focus();
		return false;
	}
	if(!num_check.test(id)){
		alert("숫자만 입력가능합니다.");
		forms.MEMBER_ID.value="";
		forms.MEMBER_ID.focus();
		return false;
	}
	return true;
}
function openConfirmId(joinform){
	var id=joinform.MEMBER_ID.value;
	var url="./MemberIDCheckAction.me?MEMBER_ID="+joinform.MEMBER_ID.value;
	
	if(id.length==0){
		alert("아이디를 입력하세요.");
		joinform.MEMBER_ID.focus();
		return false;
	}
	open(url,"confirm","toolbar=no,location=no,status=no,menubar=no,scrollbars=no,"+
			"resizeable=no,width=400,height=200");
}

function gNumCheck(){
	if(event.keyCode>=48&&event.keyCode<=57){
		return true;
	}else{
		alert("숫자만 입력가능합니다.");
		event.returnValue=false;
		joinform.MEMBER_ID.value="";
	}
}
</script>
<style>
h3{text-align:center; }
h3 > span {border:1px solid black; padding:5px 50px 5px 50px;}
div#info-div{text-align:center; margin-top:5%;}
div > span {border:1px solid black; padding:5px 50px 5px 50px; font-size:20px; }
div#form-div{text-align:center; margin-top:5%;}
label.radio{text-align:center;}
button{width:30%;}
</style>
</head>
<body>
	<h3><span>회원가입</span></h3>

	
	<div id="info-div"><span class="text-info">Man to Men 시스템을 이용해보세요!</span></div>

	<div id="form-div">
	<form id="joinform" name="joinform" action="./MemberJoinAction.me" method="post" onsubmit="return check();">
		<input type="text" name="MEMBER_ID" placeholder="학번을 입력하세요." onkeypress="gNumCheck()" maxlength="9"><br>
		<input type="password" name="MEMBER_PW" placeholder="암호를 입력하세요."><br>
		<input type="password" name="MEMBER_PW2" placeholder="암호를 확인합니다."><br>
		<input type="text" name="MEMBER_NAME" placeholder="이름을 입력하세요."><br>
		<input type="radio" name="MEMBER_ADMIN" id="optionsRadios1" value="3" checked>&nbsp멘티입니까?<br>
  		<input type="radio" name="MEMBER_ADMIN" id="optionsRadios2" value="2">&nbsp멘토입니까?<br><br><br>
		<button class="btn btn-large" type="submit">가입하기</button>
	</form>
</div>
</body>
</html>