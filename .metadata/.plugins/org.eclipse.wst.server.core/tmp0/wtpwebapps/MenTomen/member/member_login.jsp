<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MTM</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<style>
h3{text-align:center; }
h3 > span {border:1px solid black; padding:5px 50px 5px 50px;}
div#info-div{text-align:center; }
div > span {border:1px solid black; padding:5px 50px 5px 50px; font-size:20px; }
div#form-div{text-align:center; margin-top:5%;}
</style>
<script>
function gNumCheck(){
	if(event.keyCode>=48&&event.keyCode<=57){
		return true;
	}else{
		alert("숫자만 입력가능합니다.");
		loginform.MEMBER_ID.value='';
		event.returnValue=false;
	}
}
function check(){
	var idVal=loginform.MEMBER_ID.value;
	var num_check=/^[0-9]*$/;
	if(!num_check.test(idVal)){
		alert("숫자만 입력가능합니다.");
		return false;
	}
}
</script>
</head>
<body>

	<h3><span>Man To men</span></h3>

	<div id="info-div"><span class="text-info">Man to Men 시스템을 이용해보세요!</span></div>

	<div id="form-div" >
	<form name="loginform" action="./MemberLoginAction.me" method="post" onsubmit="return check()">
		<input type="text" name="MEMBER_ID" placeholder="학번을 입력하세요." onkeypress="gNumCheck()" maxlength="9"><br>
		<input type="password" name="MEMBER_PW" placeholder="암호를 입력하세요."><br>
		<button class="btn btn-large" type="submit">로그인</button>
	</form>
	</div>
	</body>
	</html>