<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%if(request.getAttribute("retVal")!=null){
	List lecList = (List)request.getAttribute("retVal.lecList");
	
	}%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="./css/mypage_home.css">

</head>
<body>
	<div class="container">
		<div class="row">
		<div id="top">
			<%@ include file="mypage_header.jsp"%>
		</div>
		
		</div>
 				<ul class="nav nav-pills nav-justified">
					<li role="presentation" class="active"><a href="./Mypage.rq">회원정보</a></li>
					<%if(level==2){ %>
					<li role="presentation"><a href="./RequestLecAction.rq">개설한 강좌</a></li>
					<%}else{ %>
					<li role="presentation"><a href="./RequestLecAction.rq">신청한 강좌</a></li>
					<%} %>
					<li role="presentation"><a href="#">강좌문의</a></li>
				</ul>
				<div class="row">
					<div class="col-md-5">
					<div id="pro_group">
						<div id="profill">
							<h2>프로필</h2>
						</div>
						<div id="pro_detail">
							<dl id="detail_dl">
								<dt>학번</dt>
								<dd><%=id %></dd>
								<dt>이름</dt>
								<dd><%=name %></dd>
								<dt>역할</dt>
								<%if(level==2){ %>
								<dd>멘토</dd>
								<%}else{ %>
								<dd>멘티</dd>
								<%} %>
								</dl>
						</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="page-header">
						<%if(level==2){ %>
						   <small>내 강좌 신청인</small>
						 <%}else{ %>
						   <small>내가 신청한 강좌</small>
						  <%} %>
							
							<form id="permitSelect" action="./Mypage.rq" class="form-inline">
							<select name="permit" onchange="permitSelect(this)">
							
							  <option selected disabled>승인여부</option>
							  <option value="2" <%=permit.equals("2") ? "selected":""%>>전체보기</option>
							  <option value="0" <%=permit.equals("0") ? "selected":""%>>대기중</option>
							  <option value="1" <%=permit.equals("1") ? "selected":""%>>승인</option>
								
							</select>
						</form>
						</div>
						<table class="table table-hover">
							<thead>
								<tr>
									<%if(level==2){ %>
									<th>신청인 학번</th>
									<%}else{ %>
									<th>개설인 학번</th>
									<%} %>
									<th>강좌제목</th>
									<%if(level==2){ %>
									<th>승인 여부</th>
									<%}else{ %>
									<th>신청취소</th>
									<%}	 %>
								</tr>
							</thead>
							<tbody>
							<%for(int i=0;i<lecList.size();i++){ 
									RequestBean Rb = (RequestBean)lecList.get(i);
									/* BoardBean Bb = (BoardBean)lecTitle.get(i); */
									%>
				
								<tr  >
									<%if(level==2){ %>
									<td><%=Rb.getMentee() %></td>
									<%}else{ %>
									<td><%=Rb.getMento() %></td>
									<%} %>
									
									<%if(Rb.getPermit()==0){ %>
									<td ><small class="text-primary">[대기중]</small><%=Rb.getSubject() %></td>
									<%}else if(Rb.getPermit()==1){%>
									<td ><small class="text-success">[승인]</small><%=Rb.getSubject() %></td>
									<%} %>
									
									<td>
									<form method="post" id="permitPost">
									<input type="hidden" value="<%=Rb.getIdx()%>" name="boardNum">
									<input type="hidden" value="<%=Rb.getMentee() %>" name="id">
									<input type="hidden" value="<%=Rb.getPermit() %>" name="permit">
									<%if(level==2 && Rb.getPermit()==0){ %>
									
									<button class="btn btn-default btn-xs" onClick="permitBtn(this.form)">
										<span class="glyphicon glyphicon-ok"></span> 허용
									</button>
									</form>
									</td>
									<%}else if(level==2 && Rb.getPermit()==1){%>
									<button class="btn btn-default btn-xs" onClick="permitBtn(this.form)">
										<span class="glyphicon glyphicon-ok"></span> 거부
									</button>
									</form>
									</td>
									<%}else if(level==3 && Rb.getPermit()==0){ %>
								
									<button class="btn permitCancel btn-default btn-xs" data-id="<%=Rb.getIdx()%>" data-toggle="modal" href="#myModal">
										<span class="glyphicon glyphicon-remove"></span> 취소
									</button>
									</td>
									<%} %>
								</tr>
							
							<%} %>
							</tbody>
						</table>
					</div>
					<div class="col-md-5">
						<div id="pro_group2">
						<p><a href="#" class="btn btn-default"><span>회원정보 수정</span></a></p>
						<p><a href="#" class="btn btn-default"><span>회원탈퇴</span>	</a></p>
						</div>
					</div>
				</div>
		</div>
	</div>
	<!-- 신청인 신청취소 버튼 클릭 -->
									<div class="modal fade" id="myModal" role="dialog">
								  <div class="modal-dialog modal-sm">
								    <div class="modal-content">
								      <div class="modal-header">
								        <h4 class="modal-title">신청취소</h4>
								      </div>
								      <div class="modal-body" value="">
								        <p>신청을 취소 하시겠습니까?</p>
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" id="remove_btn">확인</button>
								        <button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
								      </div>
									    </div><!-- /.modal-content -->
								  </div><!-- /.modal-dialog -->
								</div><!-- /.modal -->

<script>
function permitSelect(obj){
	$('#permitSelect').submit();
}

function permitBtn(obj){
	var id = obj.id.value;
	var boardNum = obj.boardNum.value;
	var permit = obj.permit.value;
	var allData=({"id":id,"boardNum":boardNum,"permit":permit});
	$.ajax({
		url:'RequestPermitAction.rq',
		type:'post',
		data:allData,
		success:function(data){	
		
		},error:function(xhr, status, error){
			alert(error);
		}
	});
	return false;
}
$(document).ready(function(){

	var Val=0;
	
	
	$(".permitCancel").click(function(){
		Val = $(this).data('id');
		$('.modal-body').val(Val);
	});
	
	$('#remove_btn').click(function(){
		$(location).attr('href',"./RequestDeleteAction.rq?id=<%=id%>&level=<%=level%>&boardNum="+Val);
	});
});
</script>
</body>
</html>