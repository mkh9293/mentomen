<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="./css/mypage_lec_view.css">
</head>
<body>
	<div class="container">
		<div class="row">
		<div id="top">
				<%@ include file="mypage_header.jsp"%>
		</div>
		<ul class="nav nav-pills nav-justified">
					<li role="presentation" ><a href="./Mypage.rq">회원정보</a></li>
					<%if(level==2){ %>
					<li role="presentation" class="active"><a href="./RequestLecAction.rq">개설한 강좌</a></li>
					<%}else{ %>
					<li role="presentation" class="active"><a href="./RequestLecAction.rq">신청한 강좌</a></li>
					<%} %>
					<li role="presentation"><a href="#">강좌문의</a></li>
				</ul>
		</div>
		<div class="row">
			<%if(level==2){
				for(int i=0;i<lecTitle.size();i++){
								BoardBean Bb = (BoardBean)lecTitle.get(i); %>
					<div class="col-md-5">
						<div id="pro_group2">
							<p><a href="./BoardList.bo?pageVal=4&boardIdx=<%=Bb.getBOARD_NUM() %>" class="btn btn-default"><span><%=Bb.getBOARD_SUBJECT()%></span></a></p>
						</div>
					</div>
			<%}}if(level==3){
				for(int i=0;i<lecList.size();i++){ 
								RequestBean Rb = (RequestBean)lecList.get(i); %>
					<div class="col-md-5">
						<div id="pro_group2">
						
							<p><a href="./BoardList.bo?pageVal=4&boardIdx=<%=Rb.getIdx() %>" class="btn btn-default"><span><%=Rb.getSubject()%></span></a></p>
					
						</div>
					</div>
			<%}} %>
		  </div>
		</div>
	</div>
</body>
</html>