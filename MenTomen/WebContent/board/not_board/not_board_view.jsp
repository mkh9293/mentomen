<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="mtm.board.db.*"%>
<%
	BoardBean board = (BoardBean) request.getAttribute("boarddata");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="./css/qna_board.css" rel="stylesheet">
</head>
<body>
	<%@ include file="/../header.jsp"%>
	<br>
	<br>
	<div class="container">
		<div class="row-fluid">
			<fieldset id="view_fieldset">
				<legend>공지게시판 글보기.</legend>
				<div>
					<span>
				<h4 id="view_h4">제목</h4>
				</span>
					<blockquote>
						<footer><%=board.getBOARD_SUBJECT()%></footer>
					</blockquote>	
				</div>
				<div>
					<span>
				<h4>작성자</h4>
					</span>
					<blockquote id="view_blockquote">
						<footer><%=board.getBOARD_NAME()%></footer>
					</blockquote>	
				</div>
				<hr>
				<div id="textarea-div">
					<%=board.getBOARD_CONTENT()%>
				</div>
				<hr>
				<div class="row-fluid" id="view_row">
					<%
						if (session.getAttribute("id") != null) {
							if (session.getAttribute("id").toString()
									.equals(board.getBOARD_NAME().toString())) {
					%>
					<button class="btn btn-default" id="change_btn">
						<span class="glyphicon glyphicon-pencil"></span> 수정
					</button>
					<button class="btn btn-default"  data-toggle="modal" data-target="#myModal">
						<span class="glyphicon glyphicon-remove"></span> 삭제
					</button>
					<%
						}
						} 
					%>
					<button class="btn btn-default" id="list_btn">
						<span class="glyphicon glyphicon-th-list"></span> 목록
					</button>
				
				</div>
			</fieldset>
		</div>
	<br><br>
	<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">삭제하기</h4>
      </div>
      <div class="modal-body">
        <p>삭제하시겠습니까?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="remove_btn">삭제</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">취소</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
		<script>
					$(document).ready(function(){
						$("#list_btn").click(function(){
							$(location).attr('href',"./BoardList.bo?pageVal=<%=request.getAttribute("pageVal")%>&page=<%=request.getAttribute("page")%>");
						});
						$("#change_btn").click(function(){
							$(location).attr('href',"./BoardModify.bo?pageVal=<%=request.getAttribute("pageVal")%>&num=<%=board.getBOARD_NUM()%>&page=<%=request.getAttribute("page")%>");
						});
						$("#remove_btn").click(function(){
							$(location).attr('href',"./BoardDelete.bo?pageVal=<%=request.getAttribute("pageVal")%>&num=<%=board.getBOARD_NUM()%>&page=<%=request.getAttribute("page")%>");
						});
					});
</script>
</body>
</html>