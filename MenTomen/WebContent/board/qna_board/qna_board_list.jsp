<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.util.*, java.text.SimpleDateFormat, mtm.board.db.*" %>
<%@ include file="../board_header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="./css/qna_board.css" rel="stylesheet">
</head>
<body>
<%@ include file="/../header.jsp"%>

<br><br>
<div id="list_container">
	<table class="table table-hover">
	 <colgroup>
	 	<col width="8%">
	 	<col width="45%">
	 	<col width="7%">
	 	<col width="25%">
	 	<col width="7%">
	 </colgroup>
	 <thead>
		<tr>
			<th>번호</th>
			<th>제목</th>
			<th>작성자</th>
			<th>작성시간</th>
			<th>조회수</th>
		</tr>
	  </thead>
	  <tbody>
	  <% 
	  for(int i=0;i<boardlist.size();i++){
		  	BoardBean bl=(BoardBean)boardlist.get(i);
	   %>
	  	 	<tr>
	  		<td><%=bl.getBOARD_NUM() %></td>
	  		<td>
	  		<div align="left">
	  		<%if(bl.getBOARD_LEV()!=0){ 
	  				for(int a=0;a<=bl.getBOARD_LEV()*2;a++){%>
	  				&nbsp;
	  				<%} %>
	  				▷
	  				<%}else{ %>
	  				▷
	  				<%} %>
	  		<a href="./BoardDetailAction.bo?pageVal=<%=request.getAttribute("pageVal")%>&num=<%=bl.getBOARD_NUM()%>&page=<%=nowpage%>">
	  			<%=bl.getBOARD_SUBJECT() %>
	  		</a>
	  		</div>
	  		</td>
	  		<td><%=bl.getBOARD_NAME()%></td>
	  		<td><%=bl.getBOARD_DATE() %></td>
	  		<td><%=bl.getBOARD_READCOUNT() %></td>
	  		 </tr>
	  	  <%} %>
	  	<tr id="tr3">
	  		<td colspan=2 >
	  		<div class="btn-group" role="group">
		  		<%if(nowpage<=1){ %>
		  		<button type="button" class="btn btn-default">이전</button>&nbsp;
		  		<%}else{ %>
		  		<button type="button" class="btn btn-default" onclick="location.href='./BoardList.bo?pageVal=<%=request.getAttribute("pageVal") %>&page=<%=nowpage-1 %>'">이전</button>&nbsp;
				<%} %>
				
				<%for(int a=startpage;a<=endpage;a++){ %>
						
				
					<%if(a==nowpage){ %>
					<button type="button" class="btn btn-default"><%=a %></button>
							
					<%}else{ %>
					<button type="button" class="btn btn-default" onclick="location.href='./BoardList.bo?pageVal=<%=request.getAttribute("pageVal") %>&page=<%=a%>'"><%=a %></button>
					&nbsp;
					<%} %>
						
			
				<%} %>
				
				<%if(nowpage>=maxpage){ %>
				<button type="button" class="btn btn-default">다음</button>
				<%}else{ %>
				<button type="button" class="btn btn-default" onclick="location.href='./BoardList.bo?pageVal=<%=request.getAttribute("pageVal") %>&page=<%=nowpage+1 %>'">다음</button>
				<%} %>
			</div>
	  		</td>
	  		<td colspan=5>
	  			<a href="./BoardWrite.bo?pageVal=<%=request.getAttribute("pageVal")%>">[글쓰기]</a>
	  		</td>
	  	</tr>
	  	</tbody>
	</table> 	
	</div>
</body>
</html>