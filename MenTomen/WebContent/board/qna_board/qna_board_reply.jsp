<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="mtm.board.db.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% BoardBean board = (BoardBean)request.getAttribute("boarddata"); %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Q&A 게시판</title>
<c:set var="contextPath" value="<%= request.getContextPath()%>"></c:set>
<link href="./css/qna_board.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src='<c:out value="${contextPath}"/>/se2/js/HuskyEZCreator.js'></script>
<script type="text/javascript" src="./js/qna_board.js"></script>
</head>
<body>
	<%@ include file="../../header.jsp"%>
	<br>
	<br>
	<div class="container">
		<div class="row-fluid">
			<form id="write_form" action="./BoardReplyAction.bo" method="post" name="boardform"
				class="form-horizontal" onsubmit="return check();">
				<input type="hidden" name="BOARD_ID" value="<%=board.getBOARD_ID()%>">
				<input type="hidden" name="BOARD_REF" value="<%=board.getBOARD_REF()%>">
				<input type="hidden" name="BOARD_LEV" value="<%=board.getBOARD_LEV()%>">
				<input type="hidden" name="BOARD_SEQ" value="<%=board.getBOARD_SEQ()%>">
				<input type="hidden" name="BOARD_NUM" value="<%=board.getBOARD_NUM()%>">
				<input type="hidden" name="page" value="<%=request.getAttribute("page")%>">
				<fieldset>
					<legend>Q&A게시판 답변 글쓰기 입니다.</legend>
					<div class="form-group">
						<label for="SUBJECT" class="col-sm-2 control-label" id="write_label">제목</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="SUBJECT"
								name="BOARD_SUBJECT" placeholder="제목을 입력하세요.">
						</div>
					</div>
					<div class="form-group">
						<label for="NAME" class="col-sm-2 control-label" id="write_label">작성자</label>
						<div class="col-sm-10">
							<%
								if (session.getAttribute("id") != null) {
									name = (int) session.getAttribute("id");
							%>
							<input type="text" name="BOARD_NAME" class="form-control"
								value="<%=name%>" id="NAME" readonly>
							<%
								} else {
							%>
							<input type="text" name="BOARD_NAME" class="form-control"
								id="NAME" placeholder="작성자명을 입력하세요.">
							<%
								}
							%>
						</div>
					</div>
					<div class="form-group">
						<label for="SUBJECT" class="col-sm-2 control-label" id="write_label">비밀번호</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="PASS"
								name="BOARD_PASS" placeholder="글비밀번호를 입력하세요.">
						</div>
					</div>
					<div id="write_textarea">
						<textarea name="BOARD_CONTENT" id="ir1" rows="18" class="span12" ></textarea>
					</div>
					<div class="row-fluid" id="write_row">
						<button class="btn btn-default"
							id="submit_btn">
							<span class="glyphicon glyphicon-ok"></span> 저장
						</button>
						<button class="btn btn-default"
							id="reset_btn">
							<span class="glyphicon glyphicon-remove"></span> 취소
						</button>
				</div>
				</fieldset>
			</form>


			<script>
				var oEditors = [];
				nhn.husky.EZCreator.createInIFrame({

					oAppRef : oEditors,
					elPlaceHolder : document.getElementById("ir1"), 
					sSkinURI : "${contextPath}/se2/SmartEditor2Skin.html",
					fCreator : "createSEditor2"
				});
			</script>
		</div>
</body>
</html>