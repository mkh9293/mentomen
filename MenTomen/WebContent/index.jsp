	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="header.jsp" %>
<link rel="stylesheet" href="./css/index.css">
<title>Insert title here</title>
</head>
<body>
<div class="container-fluid">
		<div class="row-fluid">
			<img src="./img/banner2.png" class="img-rounded">
		</div>
	</div>
	<div class="container">
		<h1>Man to men</h1>
		<h5>Man To men 이란?
 한 명의 멘토(Man)가 다수의 멘티들(men)에게 멘토링을 해준다는 의미로 학교 선배들이 진로를 고민하는 후배들에게 도움을 주고자, 본인의 프로그래밍 경험과 노하우를 나누는 프로그램입니다.	</h5>
		
		<br>
		<div id="banner-img2">
		<img src="./img/lec.png" class="img-circle" onClick="location.href='./BoardList.bo?pageVal=1'">
		<img src="./img/notice.png" class="img-circle" onClick="location.href='./BoardList.bo?pageVal=3'">
		<img src="./img/qna.png" class="img-circle" onClick="location.href='./BoardList.bo?pageVal=2'">
	</div>

	<div>
		<div style="display:table" id="table-div">
			<div style="display:table-row">
				<div style="display:table-cell" id="table-div2">
					<div style="display:table-row" >
					<div style="display:table-cell" ><img src="./img/home.jpg" onClick="location.href='http://skhu.ac.kr'"></div><div style="display:table-cell" ><img src="./img/lib.jpg"  onClick="location.href='http://library.skhu.ac.kr/index.ax'"></div>
				</div>
				<div style="display:table-row" >
					<div style="display:table-cell" ><img src="./img/e.jpg" onClick="location.href='http://ecareer.skhu.ac.kr/icons/app/cms/front.php'"></div><div style="display:table-cell" ><img src="./img/sw.jpg" onClick="location.href='http://sw.skhu.ac.kr/'"></div>
				</div>
				</div>
				<div style="display:table-cell"><img src="./img/mento.png" class="img-rounded">
			</div>
		</div>
	</div>
	</div>
</body>
</html>