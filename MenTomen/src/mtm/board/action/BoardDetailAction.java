package mtm.board.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardBean;
import mtm.board.db.BoardDAO;
import mtm.board.db.CommentBean;

public class BoardDetailAction implements Action{
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)throws Exception{
		request.setCharacterEncoding("utf8");

		PageCheck pc = PageCheck.getInstance();
		
		int num=Integer.parseInt(request.getParameter("num"));
		int pageVal=Integer.parseInt(request.getParameter("pageVal"));
		int page=Integer.parseInt(request.getParameter("page"));
		if(pageVal==4){
			int boardIdx=Integer.parseInt(request.getParameter("boardIdx"));
			request.setAttribute("boardIdx",boardIdx);
		}
		
		BoardDAO boarddao = BoardDAO.getInstance();
		boarddao.setReadCountUpdate(num,pageVal); 
		BoardBean boarddata=boarddao.getDetail(num,pageVal);
		
		List commentList = new ArrayList();
		commentList = boarddao.getCommentList(num,pageVal);
		if(boarddata==null){
			System.out.println("상세보기 실패");
			return null;
		}
		System.out.println("상세보기 성공");
		
		request.setAttribute("commentList",commentList);
		request.setAttribute("page",page);
		request.setAttribute("boarddata", boarddata);
		request.setAttribute("pageVal",pageVal);
		
		ActionForward forward=new ActionForward();
		forward.setRedirect(false);
		forward.setPath(pc.forwardViewCheck(pageVal));
		return forward;
	}
}
