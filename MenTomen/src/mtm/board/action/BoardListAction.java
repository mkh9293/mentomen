package mtm.board.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.*;

public class BoardListAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		BoardDAO boarddao=BoardDAO.getInstance();
		List boardlist= new ArrayList();
		PageCheck pc = PageCheck.getInstance();
		BoardBean boarddata=new BoardBean();
		
		int page=1;
		int limit=10;
		int refIdx=0;
		int boardIdx=0;
		
		if(request.getParameter("page")!=null){
			page=Integer.parseInt(request.getParameter("page"));
		}
		
		if(request.getParameter("boardIdx")!=null){
			boardIdx=Integer.parseInt(request.getParameter("boardIdx"));
		}
		
		int pageVal=Integer.parseInt(request.getParameter("pageVal"));
		int listcount = boarddao.getListCount(pageVal,boardIdx);
		
		boardlist = boarddao.getBoardList(page,limit,pageVal,boarddata,boardIdx);
		
		int maxpage=(int)((double)listcount/limit+0.95); //0.95를 더해서 올림 처리
		int startpage=(((int)((double)page/10+0.9))-1)*10+1;   
		int endpage=startpage+10-1;

		if(endpage>maxpage)endpage=maxpage;
		
		request.setAttribute("page",page);
		request.setAttribute("maxpage",maxpage);	
		request.setAttribute("startpage",startpage);
		request.setAttribute("endpage",endpage);
		request.setAttribute("listcount",listcount);
		request.setAttribute("boardlist",boardlist);
		request.setAttribute("pageVal",pageVal);
		request.setAttribute("boardIdx",boardIdx);
		
		ActionForward forward=new ActionForward();
		forward.setRedirect(false);
		forward.setPath(pc.forwardListCheck(pageVal));

		return forward;
	}
}
