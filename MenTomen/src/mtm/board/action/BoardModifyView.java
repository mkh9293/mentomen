 package mtm.board.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardBean;
import mtm.board.db.BoardDAO;

public class BoardModifyView implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		ActionForward forward= new ActionForward();
		request.setCharacterEncoding("utf8");
		
		BoardBean boarddata = new BoardBean();
		BoardDAO boarddao = BoardDAO.getInstance();
		PageCheck pc = PageCheck.getInstance();
		
		int num=Integer.parseInt(request.getParameter("num"));
		int page=Integer.parseInt(request.getParameter("page"));
		int pageVal=Integer.parseInt(request.getParameter("pageVal"));
		
		if(pageVal==2){
			boolean usercheck=boarddao.boardWriterCheck(num,request.getParameter("BOARD_PASS"));
			if(usercheck==true){
				boarddao.setReadCountUpdate(num,pageVal); 
				boarddata=boarddao.getDetail(num,pageVal); 
			}else{
				response.setContentType("text/html;charset=utf-8");
				PrintWriter out = response.getWriter();
				out.println("<script>");
				out.println("alert('비밀번호가 틀렸습니다.'); history.go(-1);");
				out.println("</script>");
				out.close();
				return null;
			}
		}else{
			boarddao.setReadCountUpdate(num,pageVal); 
			boarddata=boarddao.getDetail(num,pageVal); 
		}
		
		
		if(boarddata==null){
			System.out.println("(수정) 상세보기 실패");
			return null;
		}
		
		request.setAttribute("page",page);
		request.setAttribute("boarddata",boarddata);
		forward.setRedirect(false);
		forward.setPath(pc.forwardModifyCheck(pageVal));
		return forward;
	}
}
