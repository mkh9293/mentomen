package mtm.board.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardBean;
import mtm.board.db.BoardDAO;

public class BoardReplyAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("utf-8");
		ActionForward forward = new ActionForward();
		
		BoardDAO boarddao = BoardDAO.getInstance();
		BoardBean boarddata = new BoardBean();
		int result=0;
	
		boarddata.setBOARD_NUM(Integer.parseInt(request.getParameter("BOARD_NUM")));
		boarddata.setBOARD_ID(Integer.parseInt(request.getParameter("BOARD_ID")));
		boarddata.setBOARD_REF(Integer.parseInt(request.getParameter("BOARD_REF")));
		boarddata.setBOARD_LEV(Integer.parseInt(request.getParameter("BOARD_LEV")));
		boarddata.setBOARD_SEQ(Integer.parseInt(request.getParameter("BOARD_SEQ")));
		boarddata.setBOARD_SUBJECT(request.getParameter("BOARD_SUBJECT"));
		boarddata.setBOARD_PASS(request.getParameter("BOARD_PASS"));
		boarddata.setBOARD_NAME(request.getParameter("BOARD_NAME"));
		boarddata.setBOARD_CONTENT(request.getParameter("BOARD_CONTENT"));
		
		result=boarddao.boardReply(boarddata);
		if(result==0){
			System.out.println("reply 실패");
			return null;
		}
		System.out.println("reply 성공");
		forward.setRedirect(true);
		forward.setPath("./BoardDetailAction.bo?num="+result+"&pageVal="+2+"&page="+request.getParameter("page"));
		return forward;
	}
}
