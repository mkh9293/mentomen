package mtm.board.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardBean;
import mtm.board.db.BoardDAO;

public class BoardReplyView implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		ActionForward forward = new ActionForward();
		BoardDAO boarddao = BoardDAO.getInstance();
		BoardBean boarddata = new BoardBean(); 
		int num=Integer.parseInt(request.getParameter("num"));
		
		boarddata = boarddao.getDetail(num, 2);
		
		if(boarddata==null){
			System.out.println("이동 실패");
			return null;
		}
		System.out.println("이동 성공");
		request.setAttribute("boarddata",boarddata);
		request.setAttribute("page",Integer.parseInt(request.getParameter("page")));
		forward.setRedirect(false);
		forward.setPath("./board/qna_board/qna_board_reply.jsp");
		return forward;
	}
}
