package mtm.board.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardDAO;
import mtm.board.db.CommentBean;

public class CommentAddAction implements Action {

	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		BoardDAO boarddao=BoardDAO.getInstance();
		CommentBean commentdata=new CommentBean();
		ActionForward forward = new ActionForward();
		PageCheck pc = PageCheck.getInstance();
		request.setCharacterEncoding("utf-8");
		try{
			commentdata.setCOMMENT_NAME(request.getParameter("COMMENT_NAME"));	
			commentdata.setCOMMENT_CONTENTS(request.getParameter("COMMENT_CONTENT"));	
			commentdata.setCOMMENT_PASS(request.getParameter("COMMENT_PASS"));	
			commentdata.setBOARD_ID(Integer.parseInt(request.getParameter("BOARD_ID")));	
			commentdata.setBOARD_NUM(Integer.parseInt(request.getParameter("BOARD_NUM")));	
			
			boarddao.commentInsert(commentdata);
			forward.setRedirect(false);
			//forward.setPath("./BoardDetailAction.bo?num="+commentdata.getBOARD_NUM()+"&pageVal="+commentdata.getBOARD_ID()+"&page="+request.getParameter("page"));
			
			response.sendRedirect("./BoardDetailAction.bo?num="+commentdata.getBOARD_NUM()+"&pageVal="+commentdata.getBOARD_ID()+"&page="+request.getParameter("page"));
			return (ActionForward)response;
			//return forward;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
}
