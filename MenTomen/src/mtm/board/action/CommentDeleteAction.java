package mtm.board.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.board.db.BoardDAO;

public class CommentDeleteAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		
		ActionForward forward = new ActionForward();
		request.setCharacterEncoding("utf8");
		PageCheck pc = PageCheck.getInstance();
		BoardDAO boarddao = BoardDAO.getInstance();
		
		boolean result=false;
		int num=Integer.parseInt(request.getParameter("commentN"));
		int pageVal=Integer.parseInt(request.getParameter("pageVal"));
		String boardPass=request.getParameter("COMMENT_PASS");
	//	int page=Integer.parseInt(request.getParameter("page"));
	
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		boolean usercheck=false;
		usercheck=boarddao.CommentWriterCheck(num,boardPass);
		if(usercheck==true){
			result=boarddao.commentDelete(num,pageVal);
		}else{
			out.println("<script>");
			out.println("alert('비밀번호가 틀렸습니다.'); history.go(-1);");
			out.println("</script>");
			out.close();
			return null;
		}
		
		
		if(result==false){
			System.out.println("댓글 삭제 실패.");
			return null;
		}
		
		System.out.println("댓글 삭제 성공!");
		
		out.println("<script>");
		out.println("alert('댓글을 삭제하였습니다.'); history.go(0);");
		out.println("</script>");
		out.close();
		return null;
		
//		forward.setRedirect(true);
//		forward.setPath("./BoardList.bo?pageVal="+pageVal+"&num=");
//		return forward;
	}
}
