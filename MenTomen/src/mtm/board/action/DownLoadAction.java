package mtm.board.action;

import java.io.File;
import java.io.FileInputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownLoadAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		String fileName=request.getParameter("filename");
		String savePath="/boardupload";
		ServletContext context = request.getSession().getServletContext();	
		String sDownloadPath = request.getServletContext().getRealPath("/boardupload");
		String sFilePath = sDownloadPath+"\\"+fileName;
		System.out.println(sFilePath);
		byte b[]=new byte[4096];
		File oFile=new File(sFilePath);
		
		FileInputStream in = new FileInputStream(sFilePath);
		
		String sMimeType = request.getSession().getServletContext().getMimeType(sFilePath);
		System.out.println("sMimeType>>>"+sMimeType);
		
		if(sMimeType == null) sMimeType="application/octet-stream";
		response.setContentType(sMimeType);
		
		String sEncoding = new String(fileName.getBytes("euc-kr"),"8859_1");
		response.setHeader("Content-Disposition", "attachment;filename="+sEncoding);
		
		ServletOutputStream out2=response.getOutputStream();
		int numRead;
		
		while((numRead=in.read(b,0,b.length))!=-1){
			out2.write(b,0,numRead);
		}
		out2.flush();
		out2.close();
		in.close();
		return null;
	}
}
