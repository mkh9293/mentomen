package mtm.board.action;



import java.util.List;

import mtm.board.db.BoardBean;
import mtm.board.db.BoardDAO;

public class PageCheck {
	BoardDAO boarddao = BoardDAO.getInstance();
	BoardBean boarddata = null;
	private static PageCheck instance = null;
	static String path = null;
	private PageCheck(){}
	
	public static PageCheck getInstance(){
		if(instance==null){
			synchronized(PageCheck.class){
				if(instance==null){
					instance=new PageCheck();
				}
			}
		}
		return instance;
	}
	
	public String forwardWriteCheck(int pageVal){  // 글 쓰기 페이지 이동처리
		if(pageVal==1){
			path="./board/lec_board/lec_board_write.jsp";
		}else if(pageVal==2){
			path="./board/qna_board/qna_board_write.jsp";
		}else if(pageVal==3){
			path="./board/not_board/not_board_write.jsp";
		}else{
			path="./board/lec_room_board/lec_room_board_write.jsp";
		}
		return path;
	}
	
	public String forwardModifyCheck(int pageVal){
		if(pageVal==1){
			path="./board/lec_board/lec_board_modify.jsp";
		}else if(pageVal==2){
			path="./board/qna_board/qna_board_modify.jsp";
		}else if(pageVal==3){
			path="./board/not_board/not_board_modify.jsp";
		}else{
			path="./board/lec_room_board/lec_room_board_modify.jsp";
		}
		return path;
	}
	public String forwardListCheck(int pageVal){  // 글 리스트보기 처리
		if(pageVal==1){
			path="./board/lec_board/lec_board_list.jsp";
		}else if(pageVal==2){
			path="./board/qna_board/qna_board_list.jsp";
		}else if(pageVal==3){
			path="./board/not_board/not_board_list.jsp";
		}else{
			path="./board/lec_room_board/lec_room_board_list.jsp";
		}
		return path;
	}
	
	public String forwardViewCheck(int pageVal){
		if(pageVal==1){
			path="./board/lec_board/lec_board_view.jsp";
		}else if(pageVal==2){
			path="./board/qna_board/qna_board_view.jsp";
		}else if(pageVal==3){
			path="./board/not_board/not_board_view.jsp";
		}else{
			path="./board/lec_room_board/lec_room_board_view.jsp";
		}
		return path;
	}
	
	public boolean boardModify(BoardBean boarddata,int pageVal)throws Exception{
		if(pageVal==1){
			return boarddao.boardModify(boarddata,pageVal);
		}else if(pageVal==2){
			return boarddao.boardModify(boarddata,pageVal);
		}else{
			return boarddao.boardModify(boarddata,pageVal);
		}
	}
	

}
