package mtm.board.db;
public class BoardBean {
	private int BOARD_LIST_NUM;
	private int BOARD_NUM;
	private int BOARD_ID;
	private int BOARD_REF;
	private int BOARD_LEV;
	private int BOARD_SEQ;
	private int ref_idx;
	private String BOARD_SUBJECT;
	private String BOARD_CONTENT;
	private String BOARD_NAME;
	private String BOARD_DATE;
	private String BOARD_PASS;
	private String BOARD_FILE;
	private String REPORT_TIME;
	private int BOARD_READCOUNT;
	
	
	public String getREPORT_TIME() {
		return REPORT_TIME;
	}
	public void setREPORT_TIME(String rEPORT_TIME) {
		REPORT_TIME = rEPORT_TIME;
	}
	public int getBOARD_LIST_NUM() {
		return BOARD_LIST_NUM;
	}
	public void setBOARD_LIST_NUM(int bOARD_LIST_NUM) {
		BOARD_LIST_NUM = bOARD_LIST_NUM;
	}
	public int getRef_idx() {
		return ref_idx;
	}
	public void setRef_idx(int ref_idx) {
		this.ref_idx = ref_idx;
	}
	public String getBOARD_FILE() {
		return BOARD_FILE;
	}
	public void setBOARD_FILE(String bOARD_FILE) {
		BOARD_FILE = bOARD_FILE;
	}
	public int getBOARD_REF() {
		return BOARD_REF;
	}
	public void setBOARD_REF(int bOARD_REF) {
		BOARD_REF = bOARD_REF;
	}
	public int getBOARD_LEV() {
		return BOARD_LEV;
	}
	public void setBOARD_LEV(int bOARD_LEV) {
		BOARD_LEV = bOARD_LEV;
	}
	public int getBOARD_SEQ() {
		return BOARD_SEQ;
	}
	public void setBOARD_SEQ(int bOARD_SEQ) {
		BOARD_SEQ = bOARD_SEQ;
	}
	public String getBOARD_PASS() {
		return BOARD_PASS;
	}
	public void setBOARD_PASS(String bOARD_PASS) {
		BOARD_PASS = bOARD_PASS;
	}
	public String getBOARD_DATE() {
		return BOARD_DATE;
	}
	public void setBOARD_DATE(String bOARD_DATE) {
		BOARD_DATE = bOARD_DATE;
	}
	public int getBOARD_NUM() {
		return BOARD_NUM;
	}
	public void setBOARD_NUM(int bOARD_NUM) {
		BOARD_NUM = bOARD_NUM;
	}
	public String getBOARD_SUBJECT() {
		return BOARD_SUBJECT;
	}
	public void setBOARD_SUBJECT(String bOARD_SUBJECT) {
		BOARD_SUBJECT = bOARD_SUBJECT;
	}
	public String getBOARD_CONTENT() {
		return BOARD_CONTENT;
	}
	public void setBOARD_CONTENT(String bOARD_CONTENT) {
		BOARD_CONTENT = bOARD_CONTENT;
	}
	public String getBOARD_NAME() {
		return BOARD_NAME;
	}
	public void setBOARD_NAME(String bOARD_NAME) {
		BOARD_NAME = bOARD_NAME;
	}

	public int getBOARD_READCOUNT() {
		return BOARD_READCOUNT;
	}
	public void setBOARD_READCOUNT(int bOARD_READCOUNT) {
		BOARD_READCOUNT = bOARD_READCOUNT;
	}
	public int getBOARD_ID() {
		return BOARD_ID;
	}
	public void setBOARD_ID(int bOARD_ID) {
		BOARD_ID = bOARD_ID;
	}
	
	
}
