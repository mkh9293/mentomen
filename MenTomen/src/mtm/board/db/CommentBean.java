package mtm.board.db;

public class CommentBean {
	private String COMMENT_NAME;
	private String COMMENT_CONTENTS;
	private String COMMENT_PASS;
	private String TIME;
	private int BOARD_ID;
	private int BOARD_NUM;
	private int COMMENT_IDX;
	
	public int getCOMMENT_IDX() {
		return COMMENT_IDX;
	}
	public void setCOMMENT_IDX(int cOMMENT_IDX) {
		COMMENT_IDX = cOMMENT_IDX;
	}
	public String getTIME() {
		return TIME;
	}
	public void setTIME(String tIME) {
		TIME = tIME;
	}
	
	public String getCOMMENT_NAME() {
		return COMMENT_NAME;
	}
	public void setCOMMENT_NAME(String cOMMENT_NAME) {
		COMMENT_NAME = cOMMENT_NAME;
	}
	public String getCOMMENT_CONTENTS() {
		return COMMENT_CONTENTS;
	}
	public void setCOMMENT_CONTENTS(String cOMMENT_CONTENT) {
		COMMENT_CONTENTS = cOMMENT_CONTENT;
	}
	public String getCOMMENT_PASS() {
		return COMMENT_PASS;
	}
	public void setCOMMENT_PASS(String cOMMENT_PASS) {
		COMMENT_PASS = cOMMENT_PASS;
	}
	public int getBOARD_ID() {
		return BOARD_ID;
	}
	public void setBOARD_ID(int bOARD_ID) {
		BOARD_ID = bOARD_ID;
	}
	public int getBOARD_NUM() {
		return BOARD_NUM;
	}
	public void setBOARD_NUM(int bOARD_NUM) {
		BOARD_NUM = bOARD_NUM;
	}
	
	
}
