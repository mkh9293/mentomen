package mtm.member.action;

import javax.servlet.http.*;
import javax.servlet.http.HttpServletResponse;

public interface Action {
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)
	throws Exception;
}
