package mtm.member.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MemberFrontController extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet{
	
	public void doProcess(HttpServletRequest request,HttpServletResponse response)throws IOException, ServletException{
		String RequestURI=request.getRequestURI();
		String contextPath=request.getContextPath();
		String command=RequestURI.substring(contextPath.length());
		ActionForward forward=null;
		Action action=null;
		if(command.equals("/MemberIndex.me")){
			forward=new ActionForward();
			forward.setPath("./index.jsp");
		}else if(command.equals("/MemberLogin.me")){
			System.out.println("dd");
			forward=new ActionForward();
			forward.setPath("./member/member_login.jsp");
		}else if(command.equals("/Mypage.me")){
			forward=new ActionForward();
			forward.setPath("./mypage/mypage_home.jsp");
		}else if(command.equals("/MemberLogout.me")){
			action=new MemberLogoutAction();
			try{
				forward=action.execute(request, response);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(command.equals("/MemberJoin.me")){
			forward=new ActionForward();
			forward.setPath("./member/member_join.jsp");
		}else if(command.equals("/MemberLoginAction.me")){
			action=new MemberLoginAction();
			try{
				forward=action.execute(request, response);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(command.equals("/MemberJoinAction.me")){
				action=new MemberJoinAction();
				try{
					forward=action.execute(request, response);
				}catch(Exception e){
					e.printStackTrace();
				}	 
		}else if(command.equals("/MemberIDCheckAction.me")){
			action=new MemberIDCheckAction();
			try{
				forward=action.execute(request, response);
			}catch(Exception e){
				e.printStackTrace();
			}	 
	}
		if(forward!=null){
			if(forward.isRedirect()){
				response.sendRedirect(forward.getPath());
			}else{
				RequestDispatcher dispatcher=request.getRequestDispatcher(forward.getPath());
				dispatcher.forward(request,response);
			}
		}
	}
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		doProcess(request,response);
	}
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		doProcess(request,response);
	}
}


