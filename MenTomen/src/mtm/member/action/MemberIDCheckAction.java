package mtm.member.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.member.db.MemberDAO;

public class MemberIDCheckAction implements Action {
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		ActionForward forward=new ActionForward();
		int id=Integer.parseInt(request.getParameter("MEMBER_ID"));
		MemberDAO memberdao=MemberDAO.getInstance();
		int check=memberdao.confirmId(id);
		request.setAttribute("id",id);
		request.setAttribute("check",check);
		forward.setPath("./member/member_idchk.jsp");
		return forward;
	}
}
