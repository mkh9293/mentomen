package mtm.member.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.member.db.MemberBean;
import mtm.member.db.MemberDAO;

public class MemberJoinAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		request.setCharacterEncoding("UTF-8");
		MemberDAO memberdao=MemberDAO.getInstance();
		MemberBean dto=new MemberBean();
		ActionForward forward=null;
		dto.setMEMBER_ID(Integer.parseInt(request.getParameter("MEMBER_ID")));
		dto.setMEMBER_PW(request.getParameter("MEMBER_PW"));
		dto.setMEMBER_NAME(request.getParameter("MEMBER_NAME"));
		dto.setMEMBER_ADMIN(Integer.parseInt(request.getParameter("MEMBER_ADMIN")));
		memberdao.insertMember(dto);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out=response.getWriter();
		out.println("<script>");
		out.println("alert('회원가입 성공!');");
		out.println("location.href='./MemberLogin.me';");
		out.println("</script>");
		out.close();
		return forward;
	}
}
