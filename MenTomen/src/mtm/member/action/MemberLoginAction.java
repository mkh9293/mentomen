package mtm.member.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mtm.member.db.MemberDAO;

public class MemberLoginAction implements Action {
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		HttpSession session=request.getSession();
		ActionForward forward = new ActionForward();
		MemberDAO memberdao=MemberDAO.getInstance();
		
		int id=Integer.parseInt(request.getParameter("MEMBER_ID"));
		String pw=request.getParameter("MEMBER_PW");
		int check=memberdao.userCheck(id,pw);
		String name=memberdao.getName(id);
		
		if(check==1){
			int level=memberdao.levelCheck(id);
			session.setAttribute("level",level);
			session.setAttribute("name",name);
			session.setAttribute("id",id);
			if(memberdao.isAdmin(id)){
				forward.setRedirect(true);
				forward.setPath("/MemberIndex.me");
				return forward;
			}else{
				forward.setRedirect(true);
				forward.setPath("/MemberIndex.me");
				return forward;
			}
		}else if(check==0){
			response.setContentType("text/html;charset=euc-kr");
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert('비밀번호가 일치하지 않습니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
			out.close();
		}else{
			response.setContentType("text/html;charset=euc-kr");
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert('아이디가 존재하지 않습니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
			out.close();
		}
		return null;
	}
}
