package mtm.member.action;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MemberLogoutAction implements Action {
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response)throws Exception{
		HttpSession session = request.getSession();
		ActionForward forward = new ActionForward();
		session.invalidate();
		forward.setRedirect(true);
		forward.setPath("/MemberIndex.me");
		return forward;
	}
}