package mtm.member.db;

public class MemberBean {
	private int MEMBER_ID;
	private String MEMBER_PW;
	private String MEMBER_NAME;
	private int MEMBER_ADMIN;
	
	public int getMEMBER_ID() {
		return MEMBER_ID;
	}
	public void setMEMBER_ID(int mEMBER_ID) {
		MEMBER_ID = mEMBER_ID;
	}
	public String getMEMBER_PW() {
		return MEMBER_PW;
	}
	public void setMEMBER_PW(String mEMBER_PW) {
		MEMBER_PW = mEMBER_PW;
	}
	public String getMEMBER_NAME() {
		return MEMBER_NAME;
	}
	public void setMEMBER_NAME(String mEMBER_NAME) {
		MEMBER_NAME = mEMBER_NAME;
	}
	public int getMEMBER_ADMIN() {
		return MEMBER_ADMIN;
	}
	public void setMEMBER_ADMIN(int mEMBER_ADMIN) {
		MEMBER_ADMIN = mEMBER_ADMIN;
	}
	
}
