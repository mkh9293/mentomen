package mtm.member.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class MemberDAO {
	private DataSource ds;
	private Connection con=null;
	private PreparedStatement pstmt=null;
	private ResultSet rs=null;
	private static MemberDAO instance = null;
	
	private MemberDAO(){
		try{
			Context init=new InitialContext();
			ds=(DataSource)init.lookup("java:comp/env/jdbc/mssqlDB");
		}catch(Exception e){
			System.out.println("DB연결실패");
			e.printStackTrace();
		}
	}
	
	public static MemberDAO getInstance(){
		if(instance==null){
			synchronized(MemberDAO.class){
				if(instance==null){
					instance=new MemberDAO();
				}
			}
		}
		return instance;
	}

	public String getName(int id){
		String name="";
		try{
			con=ds.getConnection();
			String sql="select name from userlist where stNum=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
			name=rs.getString("name");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null) rs.close();
				if(pstmt!=null) pstmt.close();
				if(con!=null) con.close();
			}catch(Exception ex){}
		}
		return name;
	}
	
	public int levelCheck(int id){
		int level = 0;
		try{
			con=ds.getConnection();
			String sql="select level from userlist where stNum=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				level = rs.getInt("level");
			}else{
				level = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null) rs.close();
				if(pstmt!=null) pstmt.close();
				if(con!=null) con.close();
			}catch(Exception ex){}
		}
		return level;
	}
	public boolean insertMember(MemberBean mb)throws SQLException{
		String sql=null;
		boolean result=false;
		try{
			con=ds.getConnection();
			sql="insert into userlist(stNum,name,pass,level) values (?,?,?,?)";

			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,mb.getMEMBER_ID());
			pstmt.setString(2,mb.getMEMBER_NAME());
			pstmt.setString(3,mb.getMEMBER_PW());
			pstmt.setInt(4,mb.getMEMBER_ADMIN());
			pstmt.executeUpdate();
			result=true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(pstmt!=null) pstmt.close();
				if(con!=null) con.close();
			}catch(Exception ex){}
		}
		return result;
	}

	public int userCheck(int id,String pw)throws SQLException{
		String sql=null;
		int x=-1;
		try{
			con=ds.getConnection();
			sql="select pass from userlist where stNum=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				String memberpw=rs.getString("pass");
				if(memberpw.equals(pw)){
					x=1;
				}else{
					x=0;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null) rs.close();
				if(pstmt!=null)pstmt.close();
				if(con!=null)con.close();
			}catch(Exception ex){}
		}
		return x;
	}

	public boolean isAdmin(int id){
		String sql="select MEMBER_ADMIN from MEMBER where MEMBER_ID=?";
		int member_admin=0;
		boolean result=false;
		try{
			con=ds.getConnection();
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			rs.next();
			member_admin=rs.getInt("MEMBER_ADMIN");
			if(member_admin==1){
				result=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null)rs.close();
				if(pstmt!=null)pstmt.close();
				if(con!=null)con.close();
			}catch(Exception ex){}
		}
		return result;
	}

	public int confirmId(int id)throws SQLException{
		String sql=null;
		int x=-1;
		try{
			con=ds.getConnection();
			sql="select MEMBER_ID from member where MEMBER_ID=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				x=1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(rs!=null) rs.close();
				if(pstmt!=null) pstmt.close();
				if(con!=null) con.close();
			}catch(Exception ex) {}
		}
		return x;
	}
}
