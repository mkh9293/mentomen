package mtm.report.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class ReportFrontController extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet{

	protected void doProcess(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		String RequestURI=request.getRequestURI();
		String contextPath=request.getContextPath();
		String command=RequestURI.substring(contextPath.length());
		ActionForward forward=null;
		Action action=null;

			
			if(forward!=null){
				if(forward.isRedirect()){
					response.sendRedirect(forward.getPath());
				}else{
					RequestDispatcher dispatcher=request.getRequestDispatcher(forward.getPath());
					dispatcher.forward(request, response);
				}
			}
	}
		protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
			doProcess(request,response);
		}
		protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
			doProcess(request,response);
		}
}
