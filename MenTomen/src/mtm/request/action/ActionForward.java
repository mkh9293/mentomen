package mtm.request.action;

public class ActionForward {
	private boolean isRedirect=false;
	private String path=null;
	
	public boolean isRedirect(){
		return isRedirect;
	}
	public String getPath(){
		return path;
	}
	public void setPath(String b){
		path=b;
	}
	public void setRedirect(boolean b){
		isRedirect=b;
	}
}
