package mtm.request.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mtm.request.db.RequestDAO;

public class MypageView implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		ActionForward forward = new ActionForward();
		RequestDAO requestdao = new RequestDAO();
		HttpSession session = request.getSession();

		int id=(int)session.getAttribute("id");
		int level=(int)session.getAttribute("level");
		
		List lecList = new ArrayList();
		List lecTitle = new ArrayList();
		
		if(request.getParameter("permit")!=null){
			int permit;
			if(request.getParameter("permit").equals("0")||request.getParameter("permit").equals("1")){
				permit = Integer.parseInt(request.getParameter("permit"));
				lecList=requestdao.selectListRequest(id,level,permit);
			}else{
				permit = Integer.parseInt(request.getParameter("permit"));
				lecList = requestdao.getListRequest(id,level);
			}
		}else{
			lecList = requestdao.getListRequest(id,level);
		}
		//lecTitle=requestdao.getLecName(level,id);
			
		request.setAttribute("lecList",lecList);
		//request.setAttribute("lecTitle",lecTitle);
		forward.setRedirect(false);
		forward.setPath("./mypage/mypage_home.jsp");
		
		return forward;
		}
		
	}
