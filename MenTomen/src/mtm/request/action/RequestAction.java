package mtm.request.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.request.db.RequestBean;
import mtm.request.db.RequestDAO;

public class RequestAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		response.setContentType("text/html;charset=utf-8");
		int mento = Integer.parseInt(request.getParameter("mento"));
		int mentee = Integer.parseInt(request.getParameter("mentee"));
		int idx = Integer.parseInt(request.getParameter("idx"));
		String subject = request.getParameter("subject");
		int permit=0;
		
		RequestBean rq = new RequestBean();
		RequestDAO requestdao = new RequestDAO();
		rq.setIdx(idx);
		rq.setMentee(mentee);
		rq.setMento(mento);
		rq.setSubject(subject);
		rq.setPermit(permit);
		boolean result=requestdao.insertRequest(rq);
		
		PrintWriter out = response.getWriter();
		if(result==true){
			out.println("<script>");
			out.println("alert('신청이 완료되었습니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
		}else{
			out.println("<script>");
			out.println("alert('오류로 인하여 신청 실패하였습니다.');");
			out.println("history.go(-1);");
			out.println("</script>");
		}
		out.close();
		return null;
	}
}
