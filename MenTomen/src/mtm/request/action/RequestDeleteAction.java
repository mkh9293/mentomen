package mtm.request.action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.request.db.RequestDAO;

public class RequestDeleteAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		RequestDAO requestdao = new RequestDAO();
		ActionForward forward = new ActionForward();
		int id = Integer.parseInt(request.getParameter("id"));
		int boardNum = Integer.parseInt(request.getParameter("boardNum"));
		int level = Integer.parseInt(request.getParameter("level"));
		boolean result;
		result=requestdao.requestDelete(id, boardNum);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		if(result==false){
			out.println("<script>");
			out.println("alert('신청 취소가 실패하였습니다.'); history.go(-1);");
			out.println("</script>");
			out.close();
			return null;
		}
		forward.setRedirect(true);
		forward.setPath("./Mypage.rq?id="+id+"&level="+level);
		return forward;
	}
}
