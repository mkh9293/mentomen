package mtm.request.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mtm.request.db.RequestDAO;

public class RequestLecAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		RequestDAO requestdao = new RequestDAO();
		List lecTitle = new ArrayList();
		List lecList = new ArrayList();
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		
		int level =(int)session.getAttribute("level");
		if(level==2){
			String stNum = session.getAttribute("id").toString();
			lecTitle=requestdao.myLecName(stNum);
			request.setAttribute("lecTitle",lecTitle);
		}else{
			int stNum = (int)session.getAttribute("id");
			lecList = requestdao.selectListRequest(stNum,level,1);
			request.setAttribute("lecList",lecList);
		}
	
		forward.setRedirect(false);
		forward.setPath("./mypage/mypage_lec_view.jsp");
		return forward;
	}
}
