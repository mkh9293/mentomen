package mtm.request.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mtm.request.db.RequestDAO;

public class RequestPermitAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		int id = Integer.parseInt(request.getParameter("id"));
		int boardNum = Integer.parseInt(request.getParameter("boardNum"));
		int permit = Integer.parseInt(request.getParameter("permit"));
		RequestDAO requestdao = new RequestDAO();
		
		boolean result=requestdao.setPermit(id,boardNum,permit);
		if(result==false){
			System.out.println("다시시도!");
			return null;
		}
		System.out.println("성공");
		return null;
	}
}
