package mtm.request.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mtm.request.db.RequestDAO;

public class RequestPermitSelectAction implements Action{
	public ActionForward execute(HttpServletRequest request,HttpServletResponse response) throws Exception{
		HttpSession session = request.getSession();
	
		int id=(int)session.getAttribute("id");
		int level=(int)session.getAttribute("level");
		int permitSelect = Integer.parseInt(request.getParameter("permitSelect"));
		List lecList = new ArrayList();
		RequestDAO requestdao = new RequestDAO();
		lecList = requestdao.selectListRequest(id,level,permitSelect);
		response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
	    response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
	    response.getWriter().write("dd");       
	    
		return null;
	}
}
