package mtm.request.db;

public class RequestBean {
	private int mento;
	private int mentee;
	private int permit;
	private int idx;
	private int ref_idx;
	private String subject;
	
	
	public int getRef_idx() {
		return ref_idx;
	}
	public void setRef_idx(int ref_idx) {
		this.ref_idx = ref_idx;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getMento() {
		return mento;
	}
	public void setMento(int mento) {
		this.mento = mento;
	}
	public int getMentee() {
		return mentee;
	}
	public void setMentee(int mentee) {
		this.mentee = mentee;
	}
	public int getPermit() {
		return permit;
	}
	public void setPermit(int permit) {
		this.permit = permit;
	}
	
	
}
