package mtm.request.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import mtm.board.db.BoardBean;

public class RequestDAO {
	DataSource ds;
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	public RequestDAO(){
		try{
			con=DB.getConnection();
//			Context initCtx = new InitialContext();
//			Context envCtx =(Context)initCtx.lookup("java:comp/env");
//			ds=(DataSource)envCtx.lookup("jdbc/mssqlDB");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean insertRequest(RequestBean rq) throws SQLException{
		String sql = null;
		boolean result = false;
		try{
			//con = ds.getConnection();
			sql = "insert into request values(?,?,?,?,?)";
			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1,rq.getMento());
			pstmt.setInt(2,rq.getMentee());
			pstmt.setInt(3,rq.getPermit());
			pstmt.setInt(4,rq.getIdx());
			pstmt.setString(5,rq.getSubject());
			pstmt.executeUpdate();
			result=true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{ if(pstmt!=null)pstmt.close();
				 if(con!=null)con.close();
			}catch(Exception ex){}
		}
		return result;
	}
	
	public boolean requestDelete(int id,int boardNum){
		int result;
		String sql;
		try{
			sql="Delete from request where mentee=? and idx=?";
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.setInt(2, boardNum);
			result=pstmt.executeUpdate();
			if(result==0) return false;
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(pstmt!=null)pstmt.close();
				if(con!=null)con.close();
			}catch(Exception ex){}
		}
		return false;
	}
	
	public List getListRequest(int id, int level){
		List lecList = new ArrayList();
		String sql = null;
		try{
			//con = ds.getConnection();	
			if(level==3){
				sql = "select * from [request] where mentee=? order by idx asc";
			}else{
				sql = "select * from [request] where mento=? order by idx asc";
			}
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				RequestBean Rb = new RequestBean();
				Rb.setIdx(rs.getInt("idx"));
				Rb.setPermit(rs.getInt("permit"));
				if(level==2){
					Rb.setMentee(rs.getInt("mentee"));
				}else{
					Rb.setMento(rs.getInt("mento"));
				}
				Rb.setSubject(rs.getString("subject"));
				lecList.add(Rb);
			}
			return lecList;
		}catch(Exception e){
			System.out.println("listRequest 에러"+e);
		}finally{
			if(rs!=null) try{rs.close();}catch(Exception ex){}
			if(pstmt!=null) try{pstmt.close();}catch(Exception ex){}
		}
		return null;
	}
	
	public List selectListRequest(int id, int level,int permit){
		List lecList = new ArrayList();
		String sql = null;
		try{
			//con = ds.getConnection();	
			if(level==3){
				sql = "select * from [request] where mentee=? and permit=? order by permit asc";
			}else{
				sql = "select * from [request] where mento=? and permit=? order by permit asc";
			}
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.setInt(2, permit);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				RequestBean Rb = new RequestBean();
				Rb.setIdx(rs.getInt("idx"));
				Rb.setPermit(rs.getInt("permit"));
				if(level==2){
					Rb.setMentee(rs.getInt("mentee"));
				}else{
					Rb.setMento(rs.getInt("mento"));
				}
				Rb.setSubject(rs.getString("subject"));
				lecList.add(Rb);
			}
			return lecList;
		}catch(Exception e){
			System.out.println("listRequest 에러"+e);
		}finally{
			if(rs!=null) try{rs.close();}catch(Exception ex){}
			if(pstmt!=null) try{pstmt.close();}catch(Exception ex){}
		}
		return null;
	}
	
	public List myLecName(String stNum){
		List lecTitle = new ArrayList();
		String sql = null;
		try{
			//con = ds.getConnection();	
			sql = "select [title],[idx] from [lecboard] where [name]=?";
			
			pstmt=con.prepareStatement(sql);
			pstmt.setString(1, stNum);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				BoardBean Bb = new BoardBean();
				Bb.setBOARD_SUBJECT(rs.getString("title"));
				Bb.setBOARD_NUM(rs.getInt("idx"));
				lecTitle.add(Bb);
			}
			return lecTitle;
		}catch(Exception e){
			System.out.println("listRequest 에러"+e);
		}finally{
			if(rs!=null) try{rs.close();}catch(Exception ex){}
			if(pstmt!=null) try{pstmt.close();}catch(Exception ex){}
		}
		return null;
	}
	
	public List getLecName(int level,int stNum){
		List lecTitle = new ArrayList();
		String sql = null;
		try{
			//con = ds.getConnection();	
			if(level==3){
				sql = "select [title] from [lecboard] where idx in (select [idx] from [request] where [mentee]=?)";
			}else{
				sql = "select [title] from [lecboard] where idx in (select [idx] from [request] where [mento]=?)";
			}
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1, stNum);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				BoardBean Bb = new BoardBean();
				Bb.setBOARD_SUBJECT(rs.getString("title"));
				lecTitle.add(Bb);
			}
			return lecTitle;
		}catch(Exception e){
			System.out.println("listRequest 에러"+e);
		}finally{
			if(rs!=null) try{rs.close();}catch(Exception ex){}
			if(pstmt!=null) try{pstmt.close();}catch(Exception ex){}
		}
		return null;
	}

	public boolean setPermit(int id, int boardNum, int permit)throws Exception{
		String sql;
		if(permit==0){
			permit=1;
			System.out.println(permit);
		}else{
			permit=0;
			System.out.println(permit);
		}
		sql="update request set permit=? where idx=? and mentee=?";
		
		try{
			pstmt=con.prepareStatement(sql);
			pstmt.setInt(1,permit);
			pstmt.setInt(2, boardNum);
			pstmt.setInt(3, id);
			int result=pstmt.executeUpdate();
			if(result==0) return false;
			return true;
		}catch(Exception e){
			System.out.println("boardModify 에러:"+e);
		}finally{
			if(rs!=null)try{rs.close();}catch(SQLException ex){}
			if(pstmt!=null)try{pstmt.close();}catch(SQLException ex){}
		}
		return false;
	}
	
	
}
